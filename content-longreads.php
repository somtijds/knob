<?php // test if the acf fields for adding a form are filled in
	$form_added = is_singular('event') && ( get_field('add_form') && have_rows('page_section') );
	$form_title = get_field('add_form_title');
	$num = 0;
	if ( ( is_page() && have_rows('page_section') ) || ( $form_added ) ) : ?>
		
		<?php
		if ( $form_added ) { 
			$num++; 
		};
        $rows = get_field('page_section');
        $col = '';
        $num = $num + count($rows);

		if ($num > 1) : ?>
			
			<div class="section-links-wrapper ">
				
				<ul class="row section-links container">

				<?php // added form means different layout
					if ( $num % 3 === 0 ) { 
						$col = ' xs6 s4'; 
					} elseif ( $num % 4 === 0 ) { 
						$col .= 'xs6 m3'; 
					} elseif ( $num % 2 === 0 || $num === 5 ) { 
						$col = 'xs6 s4'; 
					} elseif ( ( $num > 4 ) && ( $num % 2 === 0 ) ) { 
						$col = 'two-rows xs6 m3'; 
					} elseif ( ( $num > 4 ) && ( $num % 3 === 0 ) ) { 
						$col = 'two-rows xs6 s4'; 
					} else { $col = 'three-rows xs6 m4'; }

			        foreach( $rows as $row) : ?>
			        	
			        	<?php $title = $row['section_title']; ?>
			            
			            <li class="col <?php echo $col.' best-of-'.$num; ?> section-link">
		        	      
		        	      <a href="#<?php echo sanitize_title($title); ?>" title="<?php echo __('Navigeer naar ','knob')."'".$title."'"; ?>"><?php echo $title; ?><i class="fa fa-angle-right"></i></a>

			            </li>
				    <?php endforeach; ?>
					
					<?php if ( $form_added ) : ?>
						
						<li class="col <?php echo $col; ?> section-link">
							<a href="#<?php echo sanitize_title($form_title); ?>"
								title="<?php echo __('Navigeer naar ','knob')."'".$form_title."'"; ?>">
								<?php echo $form_title; ?><i class="fa fa-angle-right"></i>
							</a>
						</li>
					<?php endif; ?>

			    </ul><!-- row -->
			
			</div><!-- section-links-wrapper-->
			<a class="back-to-top btn" href="#container"><span class="btn-text"><?php _e('Back to top','knob'); ?></span><i class="fa fa-angle-up"></i></a>
		
		<?php endif; ?>

	<?php endif; ?>