<article id="post-<?php the_ID(); ?>" role="article">

       <section class="section entry-content">
         <?php the_content(); ?>
       </section>

      <footer class="article-footer">
        <div class="single-meta">
          <small><?php _e('Geplaatst op ','knob'); ?>
            <time class="updated entry-time" datetime="<?php get_the_time('Y-m-d'); ?>" itemprop="datePublished"><?php the_time(get_option('date_format')) ; ?></time>
          </small>
        </div>
      </footer>

</article>
