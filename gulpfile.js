// Project configuration

'use strict'

var project     = '.'
  , source      = project+'/library'
  , cssbuild    = source+'/css'
  , bower       = source+'/bower_components'
  , materialize = bower+'/materialize/';

// Initialization sequence

var gulp  = require('gulp'),
    sass = require('gulp-sass'),
    cssnano = require('gulp-cssnano'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    pump = require('pump'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber'),
	  plugins = require( 'gulp-load-plugins' )( { camelize: true } );

// Tasks

gulp.task( 'default', [ 'styles','scripts','watch' ], function() {

} );

gulp.task('styles', function() {
    return gulp.src(source+'/scss/**/*.scss')

        .pipe(sourcemaps.init()) // Start Sourcemaps
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest( cssbuild ))
        .pipe(rename({suffix: '.min'}))
        .pipe(cssnano())
        .pipe(sourcemaps.write('.')) // Creates sourcemaps for minified styles
        .pipe(gulp.dest( cssbuild ))
});

gulp.task( 'scripts', function( cb ) {
  pump([
      gulp.src([
          materialize+'/js/initial.js'
          , materialize+'/js/jquery.easing.1.3.js'
          , materialize+'/js/animation'
          , materialize+'/js/velocity.min.js'
          , materialize+'/js/hammer.min.js'
          , materialize+'/js/jquery.hammer.js'
          //, materialize+'/js/jquery.timeago.min.js'
          , materialize+'/js/global.js'
          //, materialize+'/js/collapsible.js'
          //, materialize+'/js/dropdown.js'
          //, materialize+'/js/modal.js'
          , materialize+'/js/materialbox.js'
          //, materialize+'/js/parallax.js'
          //, materialize+'/js/tabs.js'
          //, materialize+'/js/tooltip.js'
          //, materialize+'/waves.js'
          //, materialize+'/js/toasts.js'
          , materialize+'/js/sideNav.js'
          , materialize+'/js/scrollspy.js'
          //, materialize+'/js/forms.js'
          , materialize+'/js/slider.js'
          //, materialize+'/js/cards.js'
          //, materialize+'/js/chips.js'
          , materialize+'/js/pushpin.js'
          //, materialize+'/js/buttons.js'
          , materialize+'/js/transitions.js'
          //, materialize+'/js/scrollFire.js'
          //, materialize+'/js/character_counter.js'
          //, materialize+'/js/carousel.js'          
          //, materialize+'/js/tapTarget.js'
          , source+'/js/scripts.js']),
    uglify(),
    plugins.concat( 'all.min.js' ),
    gulp.dest( source+'/js/')
    ],
    cb
  );
});

gulp.task( 'watch', function() {
    gulp.watch( [
      bower+'/*/{sass,scss}/*.scss'
    , bower+'/*/dist/js/*.js'
    , source+'/scss/*.scss'
    , source+'/scss/*/*.scss'
    , source+'/js/scripts.js'
  ], [ 'scripts','styles' ] );
} );
