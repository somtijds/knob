<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<!--[if IE 9]>
			<meta http-equiv="X-UA-Compatible" content="IE=8">
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<!--[if gt IE 9]>
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<![endif]-->

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo home_url(); ?>/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo home_url(); ?>/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo home_url(); ?>/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo home_url(); ?>/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo home_url(); ?>/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo home_url(); ?>/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo home_url(); ?>/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo home_url(); ?>/apple-touch-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo home_url(); ?>/apple-touch-icon-180x180.png">
		<link rel="icon" type="image/png" href="<?php echo home_url(); ?>/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="<?php echo home_url(); ?>/favicon-194x194.png" sizes="194x194">
		<link rel="icon" type="image/png" href="<?php echo home_url(); ?>/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="<?php echo home_url(); ?>/android-chrome-192x192.png" sizes="192x192">
		<link rel="icon" type="image/png" href="<?php echo home_url(); ?>/favicon-16x16.png" sizes="16x16">
		<link rel="manifest" href="/manifest.json">

		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#2b5797">
		<meta name="msapplication-TileImage" content="/mstile-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<script>
				Modernizr.load([
					{
						test : Modernizr.mq('only all'),
						nope : '<?php echo get_template_directory_uri(); ?>/library/bower_components/dest/respond.min.js'
					},
					{ test : Modernizr.flexbox,
						nope : '<?php echo get_template_directory_uri(); ?>/library/bower_components/flexie/dist/flexie.min.js'
					}
			]);
		</script>


		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>

	</head>

	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

		<div id="container">
			
			<?php get_template_part('content','header'); ?>
			
			<?php get_template_part('content','featured-slider'); ?>

			<?php if ( ! is_front_page() && ( has_post_thumbnail() || ! empty( get_field( 'header_image', $post->ID ) ) ) ) : ?>
				
				<?php get_template_part('content','featured-image'); ?>
			
			<?php endif; ?>

			<?php if ( function_exists( 'yoast_breadcrumb' ) ) : ?>

			<?php endif; //end breadcrumbs ?>

        	<?php get_template_part('content','longreads'); ?>
