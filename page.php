<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="container">
					<div class="row">

						<main id="main" class="col s12 l9" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<!-- <header class="article-header">

									remove title to give breadcrumbs title status // <h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>

								</header> <?php // end article header ?>-->

								<section class="section entry-content" itemprop="articleBody">

								<?php
								the_content();

								wp_link_pages( array(
									'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'knob' ) . '</span>',
									'after'       => '</div>',
									'link_before' => '<span>',
									'link_after'  => '</span>',
								) );
								?>

								<?php
									if ( get_field('add_page_sections')  && have_rows('page_section') ) {

										get_template_part('content','page-sections');

									} ?>


								</section><!-- end article content -->

								<footer class="article-footer cf">

								</footer>

								<?php comments_template(); ?>

							</article>

							<?php endwhile; endif; ?>

						</main>

						<?php get_sidebar(); ?>
					</div><!-- row -->

				</div>

			</div>

<?php get_footer(); ?>
