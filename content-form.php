<div class="col s12 m6">
 <article id="post-<?php the_ID(); ?>" <?php post_class( 'card' ); ?> role="article">
    <div class="card-content">
        <span class="event-title"><?php the_title(); ?></span>
    </div><!-- card-content -->
    <footer class="article-footer card-action">
        <a href="<?php echo get_the_permalink()."#inschrijven"; ?>"><?php _e('Naar het formulier','knob'); ?><i class="fa fa-angle-right"></i></a>
    </footer>
</article><!-- card -->
</div><!-- col -->
