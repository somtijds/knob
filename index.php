<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="container">
					<div class="row">
						<main id="main" class="col xxs12 m8" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
					<?php /*
							<header class="article-header">
								 <h1 class="h2 entry-title">
									<?php if( is_home() && get_option('page_for_posts') ) {
													$blog_page_id = get_option('page_for_posts');
													echo get_page($blog_page_id)->post_title;
												} else { the_title(); } ?></a>
								</h1> */ ?>
							</header>

							<?php if ( have_posts() ) {	$knob_post_count = 0; ?>

									<div class="row posts-wrapper">

									<?php	while ( have_posts() ) {	the_post(); ?>

											<?php
											   $knob_post_count++;
												/*  if ( isset($knob_post_count) && $knob_post_count <= 2 ) { */ ?>
											<?php if ( is_sticky() ) { ?>
											  <div class="col xxs12">
											    <?php get_template_part('content','post-large'); ?>
											  </div><!-- col -->
											<?php } elseif ( $knob_post_count <= 6 ) { ?>
											  <div class="col xxs12">
											    <?php get_template_part('content','post-small'); ?>
											  </div><!-- col -->
											<?php } ?>

									<?php } // endwhile ?>

									</div><!-- post_wrapper-row -->

									<?php /* bones_page_navi(); */ ?>

							<?php } else { ?>

									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'knob' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'knob' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the index.php template.', 'knob' ); ?></p>
										</footer>
									</article>

							<?php }; ?>


						</main>

					<?php get_sidebar(); ?>

			</div><!-- #inner-content -->

			</div>


<?php get_footer(); ?>
