<?php
/*
 Template Name: Home Page
 *
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="container">
					<div class="row">
						<main id="main" class="col s12 l9" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

								<?php if ( is_active_sidebar( 'home_widget_horizontal' ) ) : ?>
									<div class="home-widget-horizontal widget-area" role="complementary">
										<?php dynamic_sidebar( 'home_widget_horizontal' ); ?>
									</div><!-- #orizontal widget-->

								<?php else : ?>
									<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
										<section class="section entry-content" itemprop="articleBody">
											<article id="post-<?php the_ID(); ?>" <?php post_class( 'container' ); ?> role="article">
												<?php the_content(); ?>
											</article>
										</section>
								<?php endwhile; else : ?>
									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'knob' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'knob' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page-custom.php template.', 'knob' ); ?></p>
										</footer>
									</article>
								<?php endif; ?>
							<?php endif; ?>

									<section class="section row">
										<?php if ( is_active_sidebar( 'home_widget_recent_1' ) ) : ?>
											<div class="home-widget-recent-1 widget-area col xxs12 xs6" role="complementary">
												<?php dynamic_sidebar( 'home_widget_recent_1' ); ?>
											</div><!-- #-content-widget-->
										<?php endif; ?>
										<?php if ( is_active_sidebar( 'home_widget_recent_2' ) ) : ?>
											<div class="home-widget-recent-2 widget-area col xxs12 xs6" role="complementary">
												<?php dynamic_sidebar( 'home_widget_recent_2' ); ?>
											</div><!-- #orizontal widget-->
										<?php endif; ?>
									</section>

						</main>

						<?php get_sidebar(); ?>
					</div><!-- row -->
				</div>

			</div>


<?php get_footer(); ?>
