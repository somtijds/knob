  <div class="page-sections-wrapper" itemprop="articleBody">

    <?php // loop through the sections
     while ( have_rows('page_section') ) : the_row(); ?>

      <section class="section page-section sticker">
        <?php
        // display section values
        $section_title =  get_sub_field('section_title');
        $sanitized_section_title = sanitize_title($section_title);
        echo "<h3 class='section-title scrollspy' id='"
          .$sanitized_section_title
          ."'>"
          .$section_title
          ."</h3>"
          ."<div class='section-content'>";
        echo get_sub_field('section_content');
        echo "</div>"; ?>

      </section><!-- page-section -->

    <?php endwhile; ?>

  </div>
