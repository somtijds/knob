<?php
/*
 Template Name: Form Archive
 *
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="container">
					<div class="row">

					<main id="main" class="col s12 l8" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

						<h1 class="archive-title h2"><?php _e('Archief: formulieren','knob') ?></h1>
						<div class="row">
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

								<?php get_template_part('content','form'); ?>


							<?php endwhile; ?>
						</div>
									<?php bones_page_navi(); ?>

							<?php else : ?>

									<article id="post-not-found" class="hentry cf">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'knob' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'knob' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the custom posty type archive template.', 'knob' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</main>

					<?php get_sidebar(); ?>
				</div>
			</div>

			</div>

<?php get_footer(); ?>
