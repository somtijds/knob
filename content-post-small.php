<article id="post-<?php the_ID(); ?>" <?php post_class( 'card' ); ?> role="article">

    <?php if ( has_post_thumbnail() ) { ?>
      <div class="card-content inline-image cf">

        <div class="image-wrapper left hide-on-medium-only">
          <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"
                <?php
                  $thumb_id     = get_post_thumbnail_id();
                  $img_square_small = wp_get_attachment_image_src( $thumb_id, 'thumbnail' );
                  $thumb_alt    = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true );
                  $thumb_title  = get_post_meta( $thumb_id, '_wp_attachment_image_title', true ); ?>

                  style="background-image: url('<?php echo $img_square_small[0]; ?>');
                         background-repeat: no-repeat;
                         background-size: cover;"

          ></a><!--card-image-->
        </div><!-- image-wrapper -->

    <?php } else { ?>

    <div class="card-content cf">

    <?php } // end of thumbnail check ?>


      <div class="content-wrapper right">
        <header class="card-title article-header">
          <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
            <h1 class="h3 entry-title"><?php the_title(); ?></h1>
          </a>
        </header>
        <section class="entry-content">
          <?php if ( !empty( $post->post_excerpt) ) { echo $post->post_excerpt; }
            else if ( !empty( $subtitle ) ) { echo $subtitle; }
            else { echo knob_excerpt(get_the_id(),56); } ?>
        </section>
      </div><!-- content-wrapper-->
    </div><!-- card-content -->

    <footer class="article-footer card-action">
      <div class= "action-links-wrapper">
        <ul class="row action-links">
          <li class="col xxs6 action-link entry-meta">
            <time class="updated entry-time" datetime="' . get_the_time('Y-m-d') . '" itemprop="datePublished"><?php the_time(get_option('date_format')) ; ?></time>
          </li>
          <li class="col xxs6 action-link">
            <a href="<?php echo get_the_permalink(); ?>"
              title="<?php echo __('Navigeer naar ','knob')."'".get_the_title()."'"; ?>">
              <?php _e('Lees meer','knob'); ?><i class="fa fa-angle-right"></i>
            </a>
          </li>
        </ul><!-- row -->
        </div>
    </footer>

</article>
