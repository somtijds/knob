<?php
/*
Author: Eddie Machado
URL: http://themble.com/bones/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, etc.
*/

// LOAD BONES CORE (if you remove this, the theme will break)
require_once( 'library/bones.php' );

// CUSTOMIZE THE WORDPRESS ADMIN (off by default)
// require_once( 'library/admin.php' );

/*********************
LAUNCH BONES
Let's get everything up and running.
*********************/

function bones_ahoy() {

  //Allow editor style.
  add_editor_style( get_stylesheet_directory_uri() . '/library/css/editor-style.css' );

  // let's get language support going, if you need it
  load_theme_textdomain( 'knob', get_template_directory() . '/library/translation' );

  // USE THIS TEMPLATE TO CREATE CUSTOM POST TYPES EASILY
  require_once( 'library/event.php' );

  // launching operation cleanup
  add_action( 'init', 'bones_head_cleanup' );
  // A better title
  add_filter( 'wp_title', 'rw_title', 10, 3 );
  // remove WP version from RSS
  add_filter( 'the_generator', 'bones_rss_version' );
  // remove pesky injected css for recent comments widget
  add_filter( 'wp_head', 'bones_remove_wp_widget_recent_comments_style', 1 );
  // clean up comment styles in the head
  add_action( 'wp_head', 'bones_remove_recent_comments_style', 1 );
  // clean up gallery output in wp
  add_filter( 'gallery_style', 'bones_gallery_style' );

  // enqueue base scripts and styles
  add_action( 'wp_enqueue_scripts', 'bones_scripts_and_styles', 999 );
  // ie conditional wrapper

  // launching this stuff after theme setup
  bones_theme_support();

  // adding sidebars to Wordpress (these are created in functions.php)
  add_action( 'widgets_init', 'bones_register_sidebars' );

  // cleaning up random code around images
  add_filter( 'the_content', 'bones_filter_ptags_on_images' );
  // cleaning up excerpt
  add_filter( 'excerpt_more', 'bones_excerpt_more' );

  // temporarily remove webshop link
  add_filter('kitw_widget_link', function() {
	  return '';
  });

} /* end bones ahoy */

// let's get this party started
add_action( 'after_setup_theme', 'bones_ahoy' );


/************* OEMBED SIZE OPTIONS *************/

if ( ! isset( $content_width ) ) {
	$content_width = 680;
}

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'KNOB-header-1440', 1440, 880, true );
add_image_size( 'KNOB-header-720', 720, 440, true );
add_image_size( 'KNOB-square-640', 640, 640, true );
add_image_size( 'KNOB-square-320', 320, 320, true );

add_filter( 'image_size_names_choose', 'bones_custom_image_sizes' );

function bones_custom_image_sizes( $sizes ) {
	return array_merge( $sizes, array(
		'KNOB-header-1440' => __('1440px by 880px'),
		'KNOB-header-720' => __('720px by 440px'),
		'KNOB-square-640' => __('640px by 640px'),
		'KNOB-square-320' => __('320px by 320px'),
	) );
}

/************* THEME CUSTOMIZE *********************/

function bones_theme_customizer($wp_customize) {
  $wp_customize->remove_section('title_tagline');
  $wp_customize->remove_section('colors');
  $wp_customize->remove_section('static_front_page');
  $wp_customize->remove_section('nav');
  $wp_customize->remove_control('blogdescription');
  //$wp_customize->remove_section('background_image');
  $wp_customize->remove_control('background_image');

  //$wp_customize->get_section('colors')->title = __( 'Theme Colors' );
  $wp_customize->get_section('background_image')->title = __( 'Headers voor Nieuws en Agenda', 'knob');

  // add events image
  $wp_customize->add_setting(
  'knob_events_header_image',
	array(
		'default'	=>  '',
		'transport'  =>  'refresh'
	)
  );
  $wp_customize->add_control(
	new WP_Customize_Upload_Control(
	  $wp_customize,
	  'events_header_image',
	  array(
		  'label'	=> 'Headerafbeelding Agenda',
		  'settings' => 'knob_events_header_image',
		  'section'  => 'background_image'
	  )
	)
  );
  $wp_customize->add_setting(
	'knob_events_subtitle',
	  array(
		  'default'			=> 'Evenementen van de KNOB',
		  'sanitize_callback'  => 'knob_sanitize_subtitle',
		  'transport'		  => 'refresh'
	  )
  );
  $wp_customize->add_control(
  'events_subtitle',
	array(
		'label'	=> 'Subtitel Agendapagina',
		'type'	 => 'textarea',
		'settings' => 'knob_events_subtitle',
		'section'  => 'background_image'
	)
  );
  // add news image
  $wp_customize->add_setting(
  'knob_news_header_image',
	array(
		'default'	=>  '',
		'transport'  =>  'refresh'
	)
  );
  $wp_customize->add_control(
	new WP_Customize_Upload_Control(
	  $wp_customize,
	  'news_header_image',
	  array(
		  'label'	=> 'Headerafbeelding Nieuws',
		  'settings' => 'knob_news_header_image',
		  'section'  => 'background_image'
	  )
	)
  );
  $wp_customize->add_setting(
	'knob_news_subtitle',
	  array(
		  'default'			=> 'Nieuws van de KNOB',
		  'sanitize_callback'  => 'knob_sanitize_subtitle',
		  'transport'		  => 'refresh'
	  )
  );
  $wp_customize->add_control(
  'news_subtitle',
	array(
		'label'	=> 'Subtitel Nieuwspagina',
		'type'	 => 'textarea',
		'settings' => 'knob_news_subtitle',
		'section'  => 'background_image'
	)
  );

} // end customize register

add_action( 'customize_register', 'bones_theme_customizer' );

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
  register_sidebar(array(
	'id' => 'sidebar-webshop',
	'name' => __( 'Webshop', 'knob' ),
	'description' => __( 'The Webshop Sidebar', 'knob' ),
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h4 class="widgettitle">',
	'after_title' => '</h4>',
  ));
	register_sidebar(array(
		'id' => 'sidebar-home',
		'name' => __( 'Homepage Sidebar', 'knob' ),
		'description' => __( 'The Home Page Sidebar', 'knob' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));
  register_sidebar(array(
	'id' => 'sidebar-news',
	'name' => __( 'News Sidebar', 'knob' ),
	'description' => __( 'The News Page Sidebar.', 'knob' ),
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h4 class="widgettitle">',
	'after_title' => '</h4>',
  ));
  register_sidebar(array(
	'id' => 'sidebar-news-item',
	'name' => __( 'Single News Item Sidebar', 'knob' ),
	'description' => __( 'The News Item Sidebar.', 'knob' ),
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h4 class="widgettitle">',
	'after_title' => '</h4>',
  ));
  register_sidebar(array(
	'id' => 'sidebar-event',
	'name' => __( 'Single Event Sidebar', 'knob' ),
	'description' => __( 'The Single Event Sidebar.', 'knob' ),
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h4 class="widgettitle">',
	'after_title' => '</h4>',
  ));
  register_sidebar(array(
	'id' => 'sidebar-calendar',
	'name' => __( 'Calendar Sidebar', 'knob' ),
	'description' => __( 'The Calendar Page Sidebar.', 'knob' ),
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h4 class="widgettitle">',
	'after_title' => '</h4>',
  ));
  register_sidebar(array(
	'id' => 'home_widget_horizontal',
	'name' => __( 'Homepage Content', 'knob' ),
	'description' => __( 'This is the information presented under the home page breadcrumbs', 'knob' ),
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h4 class="section_title">',
	'after_title' => '</h4>',
  ));
  register_sidebar(array(
	'id' => 'home_widget_recent_1',
	'name' => __( 'Homepage Recent Content #1', 'knob' ),
	'description' => __( 'The first of two recent-content-blocks', 'knob' ),
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h4 class="section_title">',
	'after_title' => '</h4>',
  ));
  register_sidebar(array(
	'id' => 'home_widget_recent_2',
	'name' => __( 'Homepage Recent Content #2', 'knob' ),
	'description' => __( 'The second of two recent-content-blocks', 'knob' ),
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h4 class="section_title">',
	'after_title' => '</h4>',
  ));
  register_sidebar(array(
	'id' => 'footer-1',
	'name' => __( 'Footer 1', 'knob' ),
	'description' => __( 'For widgets that should be on the bottom of the page', 'knob' ),
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h4 class="section_title">',
	'after_title' => '</h4>',
  ));
  register_sidebar(array(
	'id' => 'footer-2',
	'name' => __( 'Footer 2', 'knob' ),
	'description' => __( 'For widgets that should be on the bottom of the page', 'knob' ),
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h4 class="section_title">',
	'after_title' => '</h4>',
  ));
  register_sidebar(array(
	'id' => 'footer-3',
	'name' => __( 'Footer 3' , 'knob' ),
	'description' => __( 'For widgets that should be on the bottom of the page', 'knob' ),
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h4 class="section_title">',
	'after_title' => '</h4>',
  ));
  register_sidebar(array(
	'id' => 'footer-4',
	'name' => __( 'Footer 4', 'knob' ),
	'description' => __( 'For widgets that should be on the bottom of the page', 'knob' ),
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h4 class="section_title">',
	'after_title' => '</h4>',
  ));

} // don't remove this bracket!


/************* COMMENT LAYOUT *********************/

// Comment Layout
function bones_comments( $comment, $args, $depth ) {
   $GLOBALS['comment'] = $comment; ?>
  <div id="comment-<?php comment_ID(); ?>" <?php comment_class('cf'); ?>>
	<article  class="cf">
	  <header class="comment-author vcard">
		<?php
		/*
		  this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
		  echo get_avatar($comment,$size='32',$default='<path_to_url>' );
		*/
		?>
		<?php // custom gravatar call ?>
		<?php
		  // create variable
		  $bgauthemail = get_comment_author_email();
		?>
		<img data-gravatar="http://www.gravatar.com/avatar/<?php echo md5( $bgauthemail ); ?>?s=40" class="load-gravatar avatar avatar-48 photo" height="40" width="40" src="<?php echo get_template_directory_uri(); ?>/library/images/nothing.gif" />
		<?php // end custom gravatar call ?>
		<?php printf(__( '<cite class="fn">%1$s</cite> %2$s', 'knob' ), get_comment_author_link(), edit_comment_link(__( '(Edit)', 'knob' ),'  ','') ) ?>
		<time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__( 'F jS, Y', 'knob' )); ?> </a></time>

	  </header>
	  <?php if ($comment->comment_approved == '0') : ?>
		<div class="alert alert-info">
		  <p><?php _e( 'Your comment is awaiting moderation.', 'knob' ) ?></p>
		</div>
	  <?php endif; ?>
	  <section class="comment_content cf">
		<?php comment_text() ?>
	  </section>
	  <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</article>
  <?php // </li> is added by WordPress automatically ?>
<?php
} // don't remove this bracket!

/************* EXCERPT FUNCTIONS *********************/

add_action( 'init', 'knob_add_excerpts_to_pages' );
function knob_add_excerpts_to_pages() {
	 add_post_type_support( 'page', 'excerpt' );
}

function custom_read_more($id) {
	return '... <a class="read-more" href="'.get_permalink($id).'">"'.__('Lees verder.','knob').'"&nbsp;&raquo;</a>';
}

function custom_read_more_elipsis($id) {
	return '... <a class="read-more" href="'.get_permalink($id).'">"'.__('Lees verder.','knob').'"&nbsp;&raquo;</a>';
}

function custom_shorter_read_more($id) {
	return '<a class="read-more" title="'.__('Lees het volledige artikel.','knob').'" href="'.get_permalink($id).'"> &raquo;</a>';
}

function custom_shorter_read_more_elipsis($id) {
	return '... <a class="read-more" title="'.__('Lees het volledige artikel.','knob').'" href="'.get_permalink($id).'">&raquo;</a>';
}

function excerpt($id,$limit) {
	$content = wp_trim_words( get_the_content($id) , $limit, custom_read_more_elipsis($id) );
	return $content;
}

function knob_excerpt($id,$limit) {
  if ( get_field('add_page_sections',$id)  && have_rows('page_section',$id) ) {
	$content = "<ul>";
	while ( have_rows('page_section',$id) ) : the_row();
	  $content .= "<li>".get_sub_field('section_title')."</li>";
	endwhile;
	$content .= "</ul>";
  }
  else {
	if(has_excerpt($id)) {
		$content = get_the_excerpt($id);
		$content .= custom_shorter_read_more($id);
	}
	else {
		$content = get_the_content($id);
		$content = wp_trim_words( $content , $limit, custom_shorter_read_more_elipsis($id) );
	}
  }
  return $content;
}


// function knob_custom_excerpt_more( $output ) {
// 	if ( ! is_attachment() ) {
// 	$new_output = str_replace('Read more','',$output);
// 	}
// 	return $new_output;
// }
// add_filter( 'get_the_excerpt', 'knob_custom_excerpt_more' );
//
// function knob_new_excerpt_more( $more ) {
// 	return '.';
// }
// add_filter('excerpt_more', 'knob_new_excerpt_more');

// function knob_custom_excerpt_length( $length ) {
// 	return 14;
// }
// add_filter( 'excerpt_length', 'knob_custom_excerpt_length');


function knob_create_header_image( $obj ) {

  $html = '';
  $src_set_1 = array();
  $src_set_2 = array();
  $src_set_3 = array();

  //check if we're on one of the archive pages requiring a header image set in the theme options
  if ( is_home() || is_post_type_archive('event') ) {
	$use_header_options = true;
	if ( is_home() ) {
	  $thumb_id = knob_get_attachment_id_from_src( get_theme_mod('knob_news_header_image') );
	};
	if ( is_post_type_archive('event') ) {
	  $thumb_id = knob_get_attachment_id_from_src( get_theme_mod('knob_events_header_image') );
	};
  }

  // check for post featured image for square img in small viewports
  if ( ! isset( $use_header_options ) && has_post_thumbnail( $obj ) ) {
		$thumb_id = get_post_thumbnail_id( $obj->ID );
  }

  // set thumbnail variables for either the post thumbs or either of the options-images
  if ( isset( $thumb_id ) ) {
		$img_full = wp_get_attachment_image_src( $thumb_id, 'full' );
		$img_landscape_large  = wp_get_attachment_image_src( $thumb_id, 'KNOB-header-1440' );
		$img_landscape_medium  = wp_get_attachment_image_src( $thumb_id, 'KNOB-header-720' );
		$img_square = wp_get_attachment_image_src( $thumb_id, 'KNOB-square-640' );
		$img_square_half = wp_get_attachment_image_src( $thumb_id, 'KNOB-square-320' );
		$img_large = wp_get_attachment_image_src( $thumb_id, 'large' ); // 1000
		$img_medium = wp_get_attachment_image_src( $thumb_id, 'medium' ); // 400
		$thumb_alt	= get_post_meta( $thumb_id, '_wp_attachment_image_alt', true );
		$thumb_title  = get_post_meta( $thumb_id, '_wp_attachment_image_title', true );
		// detect if an archive page image is set (and relevant) adn make that the fallback image
		$default_img_src = $img_square[0];
  }

  // check for horizontally cropped slider / header-image
	$header_img = get_field( 'header_image', $obj->ID );
	
  if ( !isset( $use_header_options ) && ! empty( $header_img ) ) {

			$picture_class = 'custom-header';

			// custom header image sources on large screens
			if ( $header_img['sizes']['KNOB-header-1440-height'] > 440 ) { 
				$src_set_1[] = $header_img['sizes']['KNOB-header-1440']. ' 1440w'; 
			}
			if ( $header_img['sizes']['large-height'] > 440 ) { 
				$src_set_1[] = $header_img['sizes']['large']. ' 1000w'; 
			}
			if ( $header_img['sizes']['KNOB-header-720-height'] > 440 ) {
				$src_set_1[] = $header_img['sizes']['KNOB-header-720']. ' 720w'; 
			} elseif ( isset( $img_landscape_large ) && $img_landscape_large[2] > 720 ) { 
				$src_set_1[] = $img_landscape_large[0]. " 720w"; 
			}

			// custom header on medium-sized screens
			if ( $header_img['sizes']['KNOB-header-720-width'] > 480 && $header_img['sizes']['KNOB-header-720-height'] > 440 ) {
				$src_set_2[] = $header_img['sizes']['KNOB-header-720']. ' 720w'; 
			}
			if ( $header_img['sizes']['KNOB-square-640-height'] > 440 ) { 
				$src_set_2[] = $header_img['sizes']['KNOB-square-640']. ' 640w'; 
			} elseif ( $img_square[2] == 640 ) { 
				$src_set_2[] = $img_square[0]. ' 640w'; 
			}
			if ( $header_img['sizes']['medium-height'] == 400 ) { 
				$src_set_2[] = $header_img['sizes']['medium']. ' 400w'; 
			} elseif ( isset( $img_landscape_medium ) && $img_landscape_medium[2] > 320 ) { 
				$src_set_2[] = $img_landscape_medium[0]. ' 400w'; 
			}

			// custom header image sources on small screens
			if ( $header_img['sizes']['medium-height'] == 400 ) { 
				$src_set_3[] = $header_img['sizes']['medium']. ' 400w'; 
			}
			if ( isset( $img_square_half ) && ! empty( $img_square_half ) ) { 
				$src_set_3[] .= $img_square_half[0]. ' 320w'; 
			} elseif ( $header_img['sizes']['KNOB-square-320-height'] == 320 ) { 
				$src_set_3[] = $header_img['sizes']['KNOB-square-320']. ' 320w'; 
			}

			// fallback image
			$fallback_img = '<img srcset="'.$header_img['sizes']['medium'].'" alt="'.$header_img['alt'].'" title="'.$header_img['description'].'">';
  }

  // if not, check for featured image
  elseif( !empty( $thumb_id ) ) {

			  $picture_class = 'featured-image';
			  // retrieve thumb image sources on large screens
			  if ($img_landscape_large[2] > 440 ) { $src_set_1[] = $img_landscape_large[0]. ' 1440w'; }
			  elseif ($img_landscape_medium[2] > 440 ) { $src_set_1[] = $img_landscape_medium[0]. ' 720w'; }
			  if ($img_large[2] > 440 ) { $src_set_1[] = $img_large[0]. ' 1000w'; }
			  if ($img_medium[2] > 440 ) { $src_set_1[] = $img_medium[0]. ' 720w'; }

			  // retrieve thumb image sources on medium-sized screens
			  if ($img_medium[2] > 320 ) { $src_set_2[] = $img_medium[0]. ' 720w'; }
			  if ($img_square[2] > 320 ) { $src_set_2[] = $img_square[0]. ' 640w'; }

			  // retrieve thumb img on small screens
			  if ( $img_medium && $img_medium[2] == 400 ) { $src_set_3[] = $img_medium[0]. ' 400w'; }
			  if ( $img_square_half[2] == 320 ) { $src_set_3[] = $img_square_half[0]. ' 320w'; }
		// fallback source and alt/title details
	  $fallback_img = '<img srcset="'.$default_img_src.'" alt="'.$thumb_alt.'" title="'.$thumb_title.'">';

  } // end of thumbnail check

  $html .= '<picture class="'.$picture_class.'">';

  // add srscet based on image availability
  if (!empty($src_set_1)) { $html .= '<source media="(min-width:45em)" srcset="'.implode(', ',$src_set_1).'" sizes="100vw">';}
  if (!empty($src_set_2)) { $html .= '<source media="(min-width: 30em)" srcset="'.implode(', ',$src_set_2).'" sizes="100vw">'; }
  if (!empty($src_set_3)) { $html .= '<source media="(min-width: 20em)" srcset="'.implode(', ',$src_set_3).'" sizes="100vw">'; }
  $html .= $fallback_img;

  $html .= '</picture>';

  // return html
  return $html;
};

function knob_create_header_caption( $obj ) {

	$use_header_options = false;

  // check if the custom header images for event archive and blog page need to be used instead
  if ( is_home() || is_post_type_archive('event') ) {
		
		$use_header_options = true;

		if ( is_home() ) {
			$subtitle_long = get_theme_mod( 'knob_news_subtitle' );
			$title = apply_filters('the_title',get_page( get_option('page_for_posts') )->post_title);
		}
		if ( is_post_type_archive('event') ) {
			$subtitle_long = get_theme_mod('knob_events_subtitle');
			$title = knob_fix_archives_title( get_the_archive_title() );
		}

  } else {
		$subtitle_short = get_field( 'header_subtitle_short', $obj->ID );
		$subtitle_long = get_field( 'header_subtitle_long', $obj->ID );
		$title = get_the_title( $obj );
	}
	
  $html = '';
  $permalink = get_post_permalink( $obj->ID );
  $html .= '<div class="caption-wrapper center-align">
			  <div class="caption card left-align">';

		// only create links on homepage slider
		if (is_front_page()) {
		  $html .= '<a href="'.$permalink.'" title="'.__('Direct naar','knob').' '.$title.'">';
		}
		$html.= '<h3 class="caption-header">'.$title.'</h3>';
		if (is_front_page()) {
		  $html .= '</a>'; // end of permalink
		}

		if ( !empty($subtitle_long) ) {
		  $html .= '<p class="caption-subheader hide-on-xsmall-and-down">'.$subtitle_long.'</p>';
		}

		// add little sticker for category or date when it's not an archive
		if ( ! $use_header_options ) {

		  $cats = knob_get_cat_list( $obj->ID );
		  $event_cats = knob_get_cat_list($obj->ID,'event_cat');
			if ( !empty($cats) ) {
			  if (is_single()) {
				  $html .= '<span class="card caption-category news-date">'.get_the_date( get_option('date_format'),$obj->ID ).'</span>';
			   } else {
				  $html .='<span class="card caption-category news-category">'.$cats.'</span>';
			  } // end of news-single-check
			}
			if ( !empty($event_cats) ) {
			  $html .= '<span class="card caption-category event-category">'.$event_cats.'</span>';
			}
		};
	  $html .='</div><!--caption-->
			</div><!--caption-wrapper-->';

	  return $html;
};

function knob_get_footer_col_width() {
  $n = 0;
  $c = "xxs12";
  if ( is_active_sidebar( 'footer-1' ) ) {$n++;}
  if ( is_active_sidebar( 'footer-2' ) ) {$n++;}
  if ( is_active_sidebar( 'footer-3' ) ) {$n++;}
  if ( is_active_sidebar( 'footer-4' ) ) {$n++;}
  if ($n === 2) {$c .= " s6"; }
  if ($n === 3) {$c .= " m4"; }
  if ($n === 4) {$c .= " s6 l3"; }
  return $c;
}

function knob_get_cat_list( $id, $type = 'category' ) {
	
	$term_names = array();

  $terms = wp_get_object_terms( $id, $type );
	
	foreach( $terms as $term ) {
		$term_names[] = $term->name;
	}
	
  if ( ! empty( $term_names ) ) {
		$val = implode(', ',$term_names);
		return $val;
	}
}

function knob_human_filesize($bytes, $decimals = 2) {
  $sz = 'BKMGTP';
  $factor = floor((strlen($bytes) - 1) / 3);
  return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
}

function knob_sanitize_subtitle( $input ) {
	return strip_tags( stripslashes( $input ) );
}


// oo... thanks James Lafferty!
// https://wordpress.org/support/topic/need-to-get-attachment-id-by-image-url
function knob_get_attachment_id_from_src ($image_src) {
		global $wpdb;
		$query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";
		$id = $wpdb->get_var($query);
		return $id;

	}

function knob_fix_archives_title( $title ) {
	$title = str_replace('Archieven:','', $title);
	$title = str_replace('Archives:','', $title);
	return $title;
}

// Hook my above function to the pre_get_posts action
add_action( 'pre_get_posts', 'knob_modify_main_query' );

function knob_modify_main_query( $query ) {
  if ( is_home() && $query->is_main_query() ) { // Run only on the homepage
	$query->set('posts_per_page', 8); // Show only 8 posts on the homepage only
	$query->set('max_num_pages', 1); // Show only 8 posts on the homepage only
  }
}

// Hook my above function to the pre_get_posts action
add_action( 'pre_get_posts', 'knob_modify_events_query' );

function knob_modify_events_query( $query ) {
  if ( !is_admin() && is_post_type_archive('event') && $query->is_main_query() ) { // Run only on the events archive page
	$today = date('Ymd');
	$query->set('meta_key', 'event_date');
	$query->set('orderby', 'meta_value');
	$query->set('order', 'ASC');
	$query->set('meta_query', array(

		array(
		  'key'  => 'event_date',
		  'compare' => '>=',
		  'value' => $today
		)

	  )
	);
  }
}

add_filter( 'tiny_mce_before_init', 'knob_simplify_tiny_MCE_headers');

function knob_simplify_tiny_MCE_headers( $settings ) {
	$settings['block_formats'] = 'Paragraph=p;Heading=h2;Subheading=h3';
	return $settings;
};

function knob_get_slider_objects() {
	$hero_query_args = array(
		'meta_query' => array(
			array(
		    	'key' => '_is_featured',
		    	'value' => 'yes',
			),
		),
		'post_type' => array(
			'post',
			'page',
			'event',
			'form',
		),
		'post_status' => 'publish',
		'ignore_sticky_posts' => true,
	);

	$hero_query = new WP_Query( $hero_query_args );

	if ( is_front_page() && $hero_query->have_posts() ) {
	    $slider_obj = $hero_query->posts;
		return $slider_obj;

	};
	//set_query_var( 'slider_obj', $slider_obj );
	wp_reset_query();
}


/* DON'T DELETE THIS CLOSING TAG */ ?>
