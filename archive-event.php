<?php
/*
 Template Name: Event Archive
 *
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="container">
					<div class="row">

					<main id="main" class="col s12 l8" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Event">
				<?php /*	<h1 class="archive-title h2 hide-on-small-and-up"><?php post_type_archive_title(); ?></h1> */ ?>
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

								<?php get_template_part('content','event'); ?>


							<?php endwhile; ?>

									<?php bones_page_navi(); ?>

							<?php else : ?>

									<article id="post-not-found" class="hentry cf">
										<header class="article-header">
											<h1><?php _e( 'No events found', 'knob' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'There are no events scheduled for the moment. We will keep you informed!', 'knob' ); ?></p>
										</section>
									</article>

							<?php endif; ?>

						</main>

					<?php get_sidebar(); ?>
				</div>
			</div>

			</div>

<?php get_footer(); ?>
