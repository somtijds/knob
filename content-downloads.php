<ul class="downloads">
  <?php while( have_rows('event_docs') ) { ?>

        <?php the_row();
         // vars
         $file = get_sub_field('uploaded_document');
         $mimename ="";
         if(!empty($file['mime_type'])) {
           $mime = $file['mime_type'];
           require(get_stylesheet_directory()."/library/mimearray.php");
           if (key_exists($mime, $mime_array)) {
             $mimename = "<span class='file-mime-type'>".$mime_array[$mime]."</span>";
           }
         } ?>

         <li class="download-item">
         <?php if (is_array($file)) { ?>
               <a href="<?php echo $file['url']; ?>" title="Click to download this file">
         <?php }
           if (isset($file['icon'])) { ?>
               <img src="<?php echo $file['icon']; ?>" class="file-icon left" />
         <?php	}
               else { ?>
               <i class="fa icon-file-text-alt"></i>
         <?php
             };
             if(!empty($file['name'])) { ?>
               <span class="file-name"><?php echo $file['name']; ?></span>
         <?php
             }; ?>
               </a>
         <?php $size = "";
               $filesize = knob_human_filesize(filesize(get_attached_file($file['id'])),0);
               if ($mime !== "") {
                 echo "<small class='file-info cf'>".$mimename." (".$filesize.")</small>";
                }  ?>
         <?php if (!empty($file['description'])) {
                echo "<p>".$file['description']."</p>";
         } ?>
         </li>
   <?php }; /* end while loop */ ?>
</ul>
