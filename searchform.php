<form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>">
    <div class="row">
        <input class="col xxs7 s8" type="search" id="s" name="s" value="" />
        <div class="button-wrapper col xxs5 s4">
          <button class="btn" type="submit" id="searchsubmit" ><?php _e('Search','knob'); ?></button>
        </div>
    </div>
</form>
