				<?php if ( is_home() ) {
								echo '<div id="sidebar-news" class="sidebar col xxs12 m4 hide-on-small-and-down" role="complementary">'; }
					elseif ( is_post_type_archive('event') ) {
								echo '<div id="sidebar-event" class="sidebar col xxs12 m4" role="complementary">'; }
					elseif ( is_singular('post') ) {
								echo '<div id="sidebar-single-news-item" class="sidebar col xxs12 l3 offset-l1" role="complementary">'; }
					elseif ( is_singular('event') ) {
								echo '<div id="sidebar-single-event" class="sidebar col xxs12 l3 offset-l1" role="complementary">';  }
					elseif ( is_front_page() ) {
								echo '<div id="sidebar-home" class="sidebar col xxs12 l3 hide-on-small-only" role="complementary">'; }
					elseif ( is_page( 'bulletin' ) ) {
								echo '<div id="sidebar-home" class="sidebar col xxs12 l3" role="complementary">'; } ?>

				<?php if ( is_front_page() && ( is_active_sidebar( 'sidebar-webshop' ) || is_active_sidebar( 'sidebar-home' ) ) ) : ?>
					<div class="sidebar-inner">
						<?php dynamic_sidebar( 'sidebar-webshop' ); ?>
						<?php dynamic_sidebar( 'sidebar-home' ); ?>
					</div>
				<?php elseif ( is_page( 'bulletin' ) && is_active_sidebar( 'sidebar-webshop' ) ) : ?>
					<div class="sidebar-inner">
						<?php dynamic_sidebar( 'sidebar-webshop' ); ?>
					</div>
				<?php elseif ( is_home() && is_active_sidebar( 'sidebar-news' ) ) : ?>
					<div class="sidebar-inner">
						<?php dynamic_sidebar( 'sidebar-news' ); ?>
					</div>
				<?php elseif ( is_post_type_archive('event') && is_active_sidebar( 'sidebar-calendar' ) ) : ?>
					<div class="sidebar-inner">
						<?php dynamic_sidebar( 'sidebar-calendar' ); ?>
					</div>
				<?php elseif ( is_singular('post') && is_active_sidebar( 'sidebar-news-item' ) ) : ?>
						<div class="sidebar-inner">
							<?php dynamic_sidebar( 'sidebar-news-item' ); ?>
						</div>
				<?php elseif ( is_singular('event') && is_active_sidebar( 'sidebar-event' ) ) : ?>
						<div class="sidebar-inner">
							<?php dynamic_sidebar( 'sidebar-event' ); ?>
						</div>
				<?php else : ?>

						<?php
							/*
							 * This content shows up if there are no widgets defined in the backend.
							<div class="no-widgets">
								<p><?php _e( 'This is a widget ready area. Add some and they will appear here.', 'knob' );  ?></p>
							</div>
							*/
							?>
				<?php endif; ?>
			<?php if ( is_front_page() || is_home() || is_page( 'bulletin' ) || is_post_type_archive('event') || is_singular('post') || is_singular('event') ) { ?>
				</div><!-- close sidebar -->
			<?php } ?>
