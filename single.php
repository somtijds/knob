<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="container">
					<div class="row">
						<main id="main" class="col s12 l8" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

								<?php if (is_singular('event')) {
									get_template_part('content','event-single');
								} elseif (is_singular('form')) {
									get_template_part('content','form-single');
								} else {
									get_template_part('content','post-single');
								} ?>

							<?php endwhile; ?>

							<?php else : ?>

								<article id="post-not-found" class="hentry cf">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'knob' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'knob' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the single.php template.', 'knob' ); ?></p>
										</footer>
								</article>

							<?php endif; ?>

						</main><!-- main -->

						<?php if ( !is_singular('form') ) {
							get_sidebar();
						} ?>

				</div><!-- row -->
			</div><!-- inner content -->
		</div>

<?php get_footer(); ?>
