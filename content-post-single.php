<article id="post-<?php the_ID(); ?>" <?php if( !is_singular() ) { post_class( 'card' ); } ?> role="article">

    <?php if( !is_singular('post') ) {
      $header_img = get_field('header_image');
      $subtitle = get_field('header_subtitle_long');
      if (has_post_thumbnail() || !empty($header_img)) { ?>

        <div class="card-image">
          <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
            <?php if ( !empty( $header_img ) ) { ?>

              <img
                src="<?php echo $header_img['sizes']['KNOB-header-720']; ?>"
                alt="<?php echo $header_img['alt']; ?>"
                title="<?php echo $header_img['description'] ?>"
              >

            <?php } else { ?>
              <?php the_post_thumbnail('KNOB-header-720');
            } // endif images ?>
            <span class="card-title"><?php the_title(); ?></span>
          </a>
        </div><!-- card-image -->
        <div class='card-content'>

          <?php } else { ?>
            <div class='card-content'>
              <header class="article-header card-title">
                <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><h1 class="h3 entry-title"><?php the_title(); ?></h1></a>
              </header>
          <?php }; //endif not-single-no-image ?>

          <section class="entry-content">
            <?php if (!empty($subtitle) ) { echo $subtitle; }
              else { echo get_the_excerpt(); } ?>
          </section>

        <?php } else { // endif not-single ?>

        <section class="section entry-content">
          <?php the_content(); ?>
        </section>
        <?php } ?>


  <?php if( !is_singular('post') ) { ?>
    </div><!-- card-content -->

  <footer class="article-footer card-action">
    <div class= "action-links-wrapper">
      <ul class="row action-links">
        <li class="col s6 action-link entry-meta">
          <time class="updated entry-time" datetime="' . get_the_time('Y-m-d') . '" itemprop="datePublished"><?php the_time(get_option('date_format')) ; ?></time>
        </li>
        <li class="col s6 action-link">
          <a href="<?php echo get_the_permalink(); ?>"
            title="<?php echo __('Navigeer naar ','knob')."'".get_the_title()."'"; ?>">
            <?php _e('Lees meer','knob'); ?><i class="fa fa-angle-right"></i>
          </a>
        </li>
      </ul><!-- row -->
      </div>
  </footer>
  <?php }
    else { ?>
      <div class="single-meta">
        <small><?php _e('Geplaatst op ','knob'); ?>
          <time class="updated entry-time" datetime="<?php get_the_time('Y-m-d'); ?>" itemprop="datePublished"><?php the_time(get_option('date_format')) ; ?></time>
        </small>
      </div>
    <?php } ?>
</article>
