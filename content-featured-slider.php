<?php $slider_obj = knob_get_slider_objects(); ?>

  <?php /* check if slider_obj array has more than one slide */
    if ( ! empty( $slider_obj ) && count( $slider_obj ) > 1 ) : ?>
  <div class="hero-area" id="hero">
    <div class="slider">
      <ul class="slides">

      <?php foreach( $slider_obj as $obj ) : ?>

        <li class="KNOB-slide-wrapper">

        <?php
          // check if header-image has been set and uploaded
          echo knob_create_header_image( $obj );
          echo knob_create_header_caption( $obj );
          ?>

        </li>

      <?php endforeach; ?>

      </ul><!-- slider list -->
    </div><!-- slider container -->
  </div>

  <?php endif; ?>
