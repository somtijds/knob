/*
 * Bones Scripts File
 * Author: Willem Prins | SOMTIJDS (based on the work of Eddie Machado)
 *
*/

if( typeof is_home === "undefined" ) var is_home = jQuery('body').hasClass('home');
if( typeof is_page === "undefined" ) var is_page = jQuery('body').hasClass('page');
if( typeof is_event === "undefined" ) var is_event = jQuery('body').hasClass('single-event');
if( typeof has_subs === "undefined" ) var has_subs = jQuery('.section-links-wrapper').length > 0;

/*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
*/
function updateViewportDimensions() {
	var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],ah=w.screen.availHeight,aw=w.screen.availWidth,x=w.innerWidth||e.clientWidth||g.clientWidth,y=w.innerHeight||e.clientHeight||g.clientHeight;
	return { width:x, height:y, hdiff:ah-y };
}
// setting the viewport width
var viewport = updateViewportDimensions();

// setting slider_height true / false
var mobile_slider = false;
if ( viewport.width < 900 ) { mobile_slider = true; }

/*
 * Throttle Resize-triggered Events
 * Wrap your actions in this function to throttle the frequency of firing them off, for better performance, esp. on mobile.
 * ( source: http://stackoverflow.com/questions/2854407/javascript-jquery-window-resize-how-to-fire-after-the-resize-is-completed )
*/
var waitForFinalEvent = (function () {
	var timers = {};
	return function (callback, ms, uniqueId) {
		if (!uniqueId) { uniqueId = "Don't call this twice without a uniqueId"; }
		if (timers[uniqueId]) { clearTimeout (timers[uniqueId]); }
		timers[uniqueId] = setTimeout(callback, ms);
	};
})();

// how long to wait before deciding the resize has stopped, in ms. Around 50-100 should work ok.
var timeToWaitForLast = 30;


function fixMyButton() {
	if( has_subs ) { waitForFinalEvent( function() {

		offset = viewport.height - ( $('.back-to-top').height() + 14 );
		jQuery('.back-to-top').css('top', offset+'px');

	}, timeToWaitForLast, "fixmybutton"); }
}


/* When the window is resized, we perform this function */
jQuery(window).resize(function() {
		// update the viewport, in case the window size has changed
		viewport = updateViewportDimensions();
		fixMyButton();
    // if we're on the home page, we wait the set amount (in function above) then fire the function
    if( is_home ) { waitForFinalEvent( function() {
	      // if we're above or equal to 768 fire this off
 		if( viewport.width >= 900 ) {
        	//console.log('On home page and window sized to 900 width or more.');
			mobile_slider = false;
			jQuery( '.slider, .slides, .hero-area' ).height(440);
	      } else {
	        // otherwise, let's do this instead
	        //console.log('Not on home page, or window sized to less than 900.');
			mobile_slider = true;
			jQuery( '.slider, .slides, .hero-area' ).height(320);
	    }

    }, timeToWaitForLast, "resize-above-768"); }
});

/*
 * We're going to swap out the gravatars.
 * In the functions.php file, you can see we're not loading the gravatar
 * images on mobile to save bandwidth. Once we hit an acceptable viewport
 * then we can swap out those images since they are located in a data attribute.
*/
function loadGravatars() {
  // set the viewport using the function above
  viewport = updateViewportDimensions();
  // if the viewport is tablet or larger, we load in the gravatars
  if (viewport.width >= 900) {
  jQuery('.comment img[data-gravatar]').each(function(){
    jQuery(this).attr('src',jQuery(this).attr('data-gravatar'));
  });
	}
} // end function


/*
 * Put all your regular jQuery in here.
*/
jQuery(document).ready(function($) {

	$('#menu-the-mobile-menu').prepend('<a href="#" class="button-collapsed hide-on-small-and-up"><i class="fa fa-times"></i></a>');

	$('#search-trigger').on('click', function() {
		if ( $('#search-wrapper').hasClass('hide') ) {
			$('#search-wrapper').removeClass('hide');
		}
		else {
			$('#search-wrapper').addClass('hide');
		}
	});

	if ( is_home ) {
		var slider_height = 440;
		if (mobile_slider === true) {
			slider_height = 320;
		}

		$( '.slider' ).slider( {
			full_width: true,
			height: slider_height,
			interval: 108000
			}	);

			picturefill( {
				reevaluate: true
			} );
	};
	if ( (is_page || is_event) && has_subs ) {
		var buttonheight = $('.back-to-top').height();
		var navbarheight = $('.nav-wrapper').height();
		$('.back-to-top').pushpin({
			top: viewport.height * 1.5,
			offset: viewport.height - (buttonheight + 14)
		})
		$('#container').scrollSpy();
		$('.scrollspy').scrollSpy({ offsetTop: - (navbarheight + 14 ) });

	};

	$(".button-collapse").sideNav({
		menuWidth: viewport.width,
		draggable: false,
	});

	$(".button-collapsed").on('click', function(e) {
		e.preventDefault;
		$('#sidenav-overlay').trigger('click');
		$("#sidenav-overlay").remove();
	});



  /*
   * Let's fire off the gravatar function
   * You can remove this if you don't need it
  */
  //loadGravatars();


}); /* end of as page load scripts */
