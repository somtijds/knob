<?php


// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'bones_flush_rewrite_rules' );

// Flush your rewrite rules
function bones_flush_rewrite_rules() {
	flush_rewrite_rules();
}

// let's create the function for the Event
function knob_create_event_cpt() {
	// creating (registering) the Event
	register_post_type( 'event', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Agenda', 'knob' ), /* This is the Title of the Group */
			'singular_name' => __( 'Event', 'knob' ), /* This is the individual type */
			'all_items' => __( 'All Events', 'knob' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'knob' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Event', 'knob' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'knob' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Events', 'knob' ), /* Edit Display Title */
			'new_item' => __( 'New Event', 'knob' ), /* New Display Title */
			'view_item' => __( 'View Event', 'knob' ), /* View Display Title */
			'search_items' => __( 'Search Event', 'knob' ), /* Search Event Title */
			'not_found' =>  __( 'Nothing found in the Database.', 'knob' ), /* This displays if there are no entries yet */
			'not_found_in_trash' => __( 'Nothing found in Trash', 'knob' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'A post type for adding events to the calendar', 'knob' ), /* Event Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' => 'dashicons-calendar', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'event', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'agenda', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
		) /* end of options */
	); /* end of register post type */

	/* this adds your post tags to your custom post type */
	register_taxonomy_for_object_type( 'post_tag', 'event' );

}

	// adding the function to the Wordpress init
	add_action( 'init', 'knob_create_event_cpt');

	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/

	// now let's add custom categories (these act like categories)
	register_taxonomy( 'event_cat',
		array('event'), /* if you change the name of register_post_type( 'event', then you have to change this */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Event Categories', 'knob' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Event Category', 'knob' ), /* single taxonomy name */
				'search_items' =>  __( 'Search Event Categories', 'knob' ), /* search title for taxomony */
				'all_items' => __( 'All Event Categories', 'knob' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Event Category', 'knob' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Event Category:', 'knob' ), /* parent taxonomy title */
				'edit_item' => __( 'Edit Event Category', 'knob' ), /* edit custom taxonomy title */
				'update_item' => __( 'Update Event Category', 'knob' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New Event Category', 'knob' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Event Category Name', 'knob' ) /* name title for taxonomy */
			),
			'show_admin_column' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'event-category' ),
		)
	);

	// now let's add custom tags (these act like categories)
	register_taxonomy( 'event_tag',
		array('event'), /* if you change the name of register_post_type( 'event', then you have to change this */
		array('hierarchical' => false,    /* if this is false, it acts like tags */
			'labels' => array(
				'name' => __( 'Event Tags', 'knob' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Event Tag', 'knob' ), /* single taxonomy name */
				'search_items' =>  __( 'Search Event Tags', 'knob' ), /* search title for taxomony */
				'all_items' => __( 'All Event Tags', 'knob' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Event Tag', 'knob' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Event Tag:', 'knob' ), /* parent taxonomy title */
				'edit_item' => __( 'Edit Event Tag', 'knob' ), /* edit custom taxonomy title */
				'update_item' => __( 'Update Event Tag', 'knob' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New Event Tag', 'knob' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Event Tag Name', 'knob' ) /* name title for taxonomy */
			),
			'show_admin_column' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'event-tag' ),

		)
	);

?>
