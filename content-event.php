       <?php $event_vars = get_fields($post->ID);
             $dateformatstring = "l d F Y";
             $unixtimestamp = strtotime($event_vars['event_date']);
             $event_date = date_i18n($dateformatstring, $unixtimestamp);
            $location = $event_vars['event_location_address'];
            $subtitle_short = $event_vars['header_subtitle_short'];
            if ( $event_vars['event_show_price'] ) {
              $prices = array();
              $currency_base = ' € ';
              if ($event_vars['event_price']) {
                if ($event_vars['event_price'] == 'gratis') { $currency == ''; } else { $currency = $currency_base; }
                $prices[] = $currency.$event_vars['event_price'];
              }
              if ( have_rows('discounts') ) {
                 while ( have_rows('discounts') ) : the_row();
                  $discount_description = get_sub_field('discount_description');
                  $discount_price = get_sub_field('discount_price');
                  if (!empty($discount_description) && !empty($discount_price)) {
                    if ($discount_price == 'gratis') { $currency == ''; } else { $currency = $currency_base; }
                    $prices[] = $currency.$discount_price;
                  }
                  endwhile;
              };
              $price = implode(' / ',$prices);
            }
            $show_map = $event_vars['kaart_weergeven'];
        ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class( 'card' ); ?> role="article">

        <div class="row">

          <div class="image-col col xxs12 s6">
              <a class="event-image" href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"

                  <?php if ( has_post_thumbnail() ) {

                      $thumb_id     = get_post_thumbnail_id( get_the_ID() );
                      $img_square = wp_get_attachment_image_src( $thumb_id, 'KNOB-square-640' );
                      $img_square_small = wp_get_attachment_image_src( $thumb_id, 'KNOB-square-320' );
                      $thumb_alt    = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true );
                      $thumb_title  = get_post_meta( $thumb_id, '_wp_attachment_image_title', true ); ?>

                      style="background-image: url('<?php echo $img_square[0]; ?>');
                             background-repeat: no-repeat;
                             background-size: cover;"
                    <?php  } // end of thumbnail check ?>
              ></a><!--card-image-->
        </div><!-- image-col -->

        <div class="content-col col xxs12 s6">
          <div class="card-content">
            <header class="card-title article-header">
              <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                <h1 class="h3 entry-title"><?php the_title(); ?></h1>
                <?php if (!empty($subtitle_short)) : ?>
                <?php endif; ?>
              </a>
            </header>
            <p class="subheader"><?php echo $subtitle_short; ?></p>

            <table class="table entry-details">
              <tbody>
              <?php if( isset($event_date) ) : ?>
                <tr>
                  <td><?php _e('Datum:','knob'); ?></td>
                  <td><?php echo $event_date; ?></td>
                </tr>
              <?php endif;
                if( isset($timestart) ) : ?>
                <tr>
                  <td><?php _e('Tijd:','knob'); ?></td>
                  <td><?php echo $timestart; ?></td>
                </tr>
              <?php endif;
                if( isset($location) ) : ?>
                <tr>
                  <td><?php _e('Locatie:','knob'); ?></td>
                  <td><?php echo $location; ?></td>
                </tr>
              <?php endif;
                if( isset($price) && $price !== '' ) : ?>
                <tr>
                  <td><?php _e('Kosten:','knob'); ?></td>
                  <td><?php echo $price; ?></td>
                </tr>
              <?php endif; ?>
              </tbody>
            </table>
          </div><!-- card-content -->
        </div><!-- content-col -->

      </div><!-- row -->

      <footer class="article-footer card-action">
        <div class= "action-links-wrapper">
          <ul class="row action-links">
            <li class="col xxs6 action-link entry-meta">

              <span class="entry-category"><?php echo knob_get_cat_list(get_the_ID(),'event_cat'); ?></span>

              <?php /*echo get_the_permalink();
                if ( !empty($event_vars['add_form_title'] ) ) { echo "#".sanitize_title($event_vars['add_form_title']); }
                echo "; ?>".__('Direct aanmelden','knob')."</a>"; */?>
            </li>
            <li class="col xxs6 action-link">
              <a href="<?php echo get_the_permalink(); ?>"
                title="<?php echo __('Navigeer naar ','knob')."'".get_the_title()."'"; ?>">
                <?php _e('Lees meer','knob'); ?><i class="fa fa-angle-right"></i>
              </a>
            </li>
          </ul><!-- row -->
          </div>
      </footer>

</article>
