

			<footer class="page-footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

				<div id="inner-footer" class="container">

					<?php if ( is_active_sidebar('footer-1') || is_active_sidebar('footer-2') || is_active_sidebar('footer-3') || is_active_sidebar('footer-4') ) { $footer = true; ?>
						<div class="row">
					<?php }; ?>

							<?php if ( is_active_sidebar('footer-1' ) ) : ?>
								<div class="widget-wrapper col <?php echo knob_get_footer_col_width(); ?>">
									<?php dynamic_sidebar( 'footer-1' ); ?>
								</div>
							<?php endif; ?>
							<?php if ( is_active_sidebar('footer-2' ) ) : ?>
								<div class="widget-wrapper col <?php echo knob_get_footer_col_width(); ?>">
									<?php dynamic_sidebar( 'footer-2' ); ?>
								</div>
							<?php endif; ?>
							<?php if ( is_active_sidebar('footer-3' ) ) : ?>
								<div class="widget-wrapper col <?php echo knob_get_footer_col_width(); ?>">
									<?php dynamic_sidebar( 'footer-3' ); ?>
								</div>
							<?php endif; ?>
							<?php if ( is_active_sidebar('footer-4' ) ) : ?>
								<div class="widget-wrapper col <?php echo knob_get_footer_col_width(); ?>">
									<?php dynamic_sidebar( 'footer-4' ); ?>
								</div>
							<?php endif; ?>

					<?php if ( $footer ) : ?>
							</div>
					<?php endif; ?>

				</div>
				<div class="footer-copyright source-org">
					<div class="container">
						<?php $privacy_page = get_option('wp_page_for_privacy_policy'); ?>
					<p class="source-org copyright"><div class="copy-line-or-block">&copy; <?php echo date('Y'); ?></div> <?php bloginfo( 'name' ); ?>.
					<?php if ( ! empty( $privacy_page ) ) : ?>
					| <a href="<?php echo esc_url( get_the_permalink( $privacy_page ) ); ?>" title="<?php echo esc_attr( get_the_title( $privacy_page ) ); ?>"><?php echo esc_html( get_the_title( $privacy_page ) ); ?></a>
					<?php endif; ?></p>
					</div>
				</div>
			</footer>

		</div>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->
