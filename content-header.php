<header class="header" role="banner" itemscope itemtype="http://schema.org/WPHeader">
	<div class="navbar-fixed">
		<nav role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
			<div class="nav-wrapper">
				<a class='right' id='search-trigger' href='#'><i class="fa fa-search"></i></a>
				<a itemscope itemtype="http://schema.org/Organization" class="brand-logo" href="<?php echo home_url(); ?>" rel="nofollow">KNOB</a>
				<a href="#" data-activates="menu-the-mobile-menu" class="button-collapse hide-on-med-and-up"><i class="fa fa-bars"></i></a>

					<?php wp_nav_menu(array(
			         'container' => false,                           // remove nav container
			         'container_class' => '',                 // class of container (should you choose to use it)
			         'menu' => __( 'The Main Menu', 'knob' ),  // nav name
			         'menu_class' => 'nav top-nav hide-on-med-and-down',               // adding custom nav class
			         'theme_location' => 'main-nav',                 // where it's located in the theme
			         'before' => '',                                 // before the menu
		               'after' => '',                                  // after the menu
		               'link_before' => '',                            // before each link
		               'link_after' => '',                             // after each link
		               'depth' => 0,                                   // limit the depth of the nav
			         'fallback_cb' => ''                             // fallback function (if there is one)
					)); ?>
						<?php wp_nav_menu(array(
								'container' => false,                        		   // remove nav container
								'container_class' => '',                					 // class of container (should you choose to use it)
								'menu' => __( 'The Mobile Menu', 'knob' ),  // nav name
								'menu_class' => 'side-nav center',               // adding custom nav class
								'theme_location' => 'mobile-nav',                 // where it's located in the theme
								'before' => '',                                 // before the menu
											'after' => '',                                  // after the menu
											'link_before' => '',                            // before each link
											'link_after' => '',                             // after each link
											'depth' => 0,                                   // limit the depth of the nav
								'fallback_cb' => ''                             // fallback function (if there is one)
						)); ?>
			</div>
			<div id='search-wrapper' class='hide'><?php include('searchform.php'); ?></div>

		</nav>
	</div>
</header>