<?php
/**
 * Template Name: Webwinkel
 *
 **/
?>

<?php get_header(); ?>
			<div id="content">

				<div id="inner-content" class="container">
					<div class="row">
						<main id="main" class="col xxs12" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
								<section class="intro row">

								<?php /* <header class="article-header">

									<h1 class="page-title"><?php the_title(); ?></h1>

								</header> */ ?>
								<?php
									if ( get_field('add_page_sections')  && have_rows('page_section') ) { ?>
										<div class="col xxs12 m8 sections-wrapper">
										<?php // loop through the sections
										$section_rows = get_field('page_section');
						        $num = count($section_rows);
										$index = 1;
										$last = $num - 1;
										foreach ($section_rows as $row) {
						        	while ( $index < $num ) : ?>
												<div class="section page-section">
													<?php
													// display section values
													$sanitized_section_title = sanitize_title($row['section_title']);
													echo "<h3 class='section-title scrollspy' id='"
														.$sanitized_section_title
														."'>"
														.$row['section_title']
														."</h3>"
														."<div class='section-content'>";
													echo $row['section_content'];
													echo "</div>"; ?>

											</div><!-- page-section -->
										<?php $index++; endwhile; }
									}
									else { ?>
											<section class="section entry-content" itemprop="articleBody">
												<?php
													// the content (pretty self explanatory huh)
													//the_content();

													wp_link_pages( array(
														'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'knob' ) . '</span>',
														'after'       => '</div>',
														'link_before' => '<span>',
														'link_after'  => '</span>',
													) );
												?>
											</section>
									<?php } ?>
								</div> <!-- col sections-wrapper -->
							</div> <!-- row -->

							<?php if ( have_rows('webshop_items') ) { ?>
								<section class="webshop row">
								<?php	$rows = get_field('webshop_items');
									foreach( $rows as $row) {
										$title = $row['item_title'];
										$contents = apply_filters('the_content',$row['item_contents']);
										$image = $row['item_thumb'];
										$link = $row['item_link'];
										$price = $row['item_price'];
										?>
										<div class="col xxs12 m6 shop-item">
											<div class="row">
												<div class="col xxs12 xs6 s4 image-col">
													<div class="image-wrapper card cf">
														<img src="<?php echo $image[sizes]['medium']; ?>">
													</div>
												</div>
												<div class="col xxs12 xs6 s8">
													<div class="content-wrapper">
													<?php if ( isset($title) ) : ?>
														<h1 class="h3 entry-title"><?php echo $title; ?></h1>
														<?php endif;
															if ( isset($price) ) : ?>
															<p class="item-price"><?php echo $price; ?></p>
															<?php endif;
															if ( isset($contents) ) : echo $contents; endif; ?>
													</div>
													<ul class="item-buttons">
														<li>
															<a class="btn" href='<?php echo $link; ?>' target='_blank'
																title='<?php _e('Naar volledige inhoudsopgave op bulletin.knob.nl','knob'); ?>'>
																<?php _e('Meer info','knob'); ?>
															</a>
														</li>
														<?php /*<li>
															<a class="btn" href='<?php echo "mailto:"
																.eae_encode_str('name@domain.com')
																."?subject=Bestelling "
																.$title; ?>'><?php _e('Bestellen','knob'); ?>
															</a>
														</li> */ ?>
													</ul>
												</div>
											</div>
										</div><!-- col -->
									<?php } ?>
								</section><!-- row -->
							<?php } ?>
							<?php if ( get_field('add_page_sections')  && have_rows('page_section') ) { ?>
							<div class="row">
								<div class="page-ections-wrapper col xxs12 m8">
									<div class="section page-section">
											<?php
											// display section values

											$sanitized_section_title = sanitize_title($section_rows[$last]['section_title']);
											echo "<h3 class='section-title scrollspy' id='"
												.$sanitized_section_title
												."'>"
												.$section_rows[$last]['section_title']
												."</h3>"
												."<div class='section-content'>";
												echo $section_rows[$last]['section_content'];
											echo "</div>"; ?>

									</div><!-- page-section -->
								</div> <!-- col sections-wrapper -->
							</div> <!-- row -->
							<?php } // endif ?>
								<footer class="article-footer">

                  <?php the_tags( '<p class="tags"><span class="tags-title">' . __( 'Tags:', 'knob' ) . '</span> ', ', ', '</p>' ); ?>

								</footer>

								<?php comments_template(); ?>

							</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'knob' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'knob' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page-custom.php template.', 'knob' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>



						</main>

						<?php /* get_sidebar(); */ ?>
					</div><!-- row -->

				</div>

			</div>


<?php get_footer(); ?>
