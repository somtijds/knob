<?php $event_vars = get_fields($post->ID);
    $dateformatstring = "l d F Y";
    $unixtimestamp = strtotime($event_vars['event_date']);
    $event_date = date_i18n($dateformatstring, $unixtimestamp);
    $timestart = $event_vars['event_time_start'];
    $timestop = $event_vars['event_time_end'];
    $location = $event_vars['event_location_address'];

   if ( $event_vars['event_show_price'] ) {
     $prices = "";
     $currency_base = ' € ';
     if ($event_vars['event_price']) {
       if ($event_vars['event_price'] == 'gratis') { $currency == ''; } else { $currency = $currency_base; }
       $prices .= "<li>".$currency.$event_vars['event_price']."</li>";
     }
     if ( have_rows('discounts') ) {
       while ( have_rows('discounts') ) : the_row();
         $description = get_sub_field('discount_description');
         $price = get_sub_field('discount_price');
         if (!empty($description) && !empty($price)) {
           if ($price == 'gratis') { $currency == ''; } else { $currency = $currency_base; }
           $prices .= "<li>".$description.": ".$currency.$price."</li>";
         }
       endwhile;
     };
   }
   /*================== EVENT DETAILS ===================*/

       ?>

 <article id="post-<?php the_ID(); ?>" <?php post_class( 'event' ); ?> role="article">

      <section class="section entry-details">
        <table class="table">
          <thead>
              <th colspan="2">
                <h4 class="event-title"><?php the_title(); ?></h4>
              </th>
          </thead>
          <tbody>
          <?php if( isset($event_date) ) { ?>
          <tr>
            <td><?php _e('Datum:','knob'); ?></td>
            <td><?php echo $event_date; ?></td>
          </tr>
          <?php }
          if( isset($timestart) ) { ?>
          <tr>
            <td><?php _e('Tijd:','knob'); ?></td>
            <td><?php echo $timestart; ?><?php if ( isset($timestop) ) { echo " - ".$timestop; } ?></td>
          </tr>
          <?php }
          if( isset($location) ) { ?>
          <tr>
            <td><?php _e('Locatie:','knob'); ?></td>
            <td><?php echo $location; ?>
          </tr>
          <?php }
          if( isset($prices) && $prices !== '')  { ?>
          <tr>
            <td><?php _e('Kosten:','knob'); ?></td>
            <td><?php echo "<ul class='price-list'>".$prices."</ul>"; ?></td>
          </tr>
          <?php }
          if( $event_vars['add_form'] ) : ?>
              <tr>
                <td><?php _e('Komt u ook?','knob'); ?></td>
                <td><a class="btn" href="#<?php echo sanitize_title($event_vars['add_form_title']); ?>"><?php _e('Aanmelden','knob'); ?></a></td>
              </tr>
          <?php endif; ?>
        </tbody>
      </table>
    </section>

<?php /*================== MAIN CONTENT===================*/ ?>


    <section class="section entry-content" itemprop="articleBody">
        <?php the_content(); ?>
    </section>

<?php /*================== PAGE SECTIONS ===================*/ ?>


    <?php
      if ( get_field('add_page_sections')  && have_rows('page_section') ) {

        get_template_part('content','page-sections');

      } ?>

<?php /*================== EVENT LOCATION ( MAP ) ===================*/ ?>


      <?php /* if ( isset($event_vars['event_location_lat_long']) && isset($event_vars['event_show_map']) ) { ?>
        <ul class="collapsible" data-collapsible="accordion">
          <li>
            <div class="collapsible-header"><?php _e('Toon locatie op de kaart','knob'); ?><i class="fa fa-angle-right"></i></div>
            <div class="collapsible-body"><p><?php _e('Hier komt de kaart' ?></p></div>
          </li>
        </ul>
      <?php } */ ?>

<?php /*================== EVENT DOCUMENTS ===================*/ ?>

      <?php if ( $event_vars['add_event_files'] && have_rows('event_docs') ) { ?>
      <section class="event-downloads">
        <h4 class='section-title'><?php _e('Downloads','knob'); ?></h4>
          <?php get_template_part('content','downloads'); ?>
      </section>
      <?php }; ?>

<?php /*================== EVENT FORM ===================*/ ?>

      <?php if ( $event_vars['add_form'] ) : ?>

            <div class="section page-section">
                <?php
                // display section values

                $sanitized_section_title = sanitize_title($event_vars['add_form_title']);
                echo "<h3 class='section-title scrollspy' id='"
                  .$sanitized_section_title
                  ."'>"
                  .$event_vars['add_form_title']
                  ."</h3>"
                  ."<div class='section-content'>";

			  	if ( empty( $event_vars['add_eventbrite_id'] ) ) {
    				echo FrmFormsController::show_form($event_vars['add_formidable_id'], '', false, true);
				} else {
					//echo '<p><a class="btn" href="' . $event_vars['add_eventbrite_url'] . '" target="_blank">Bestel een ticket</a></p>';
					echo '<p><iframe  src="//eventbrite.nl/tickets-external?eid=' . $event_vars['add_eventbrite_id'] . '&ref=etckt" frameborder="0" height="400" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="auto" allowtransparency="true"></iframe><p>';
					//echo '<iframe src="' . $event_vars['add_eventbrite_id'] . ' frameborder="0" height="1000" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="auto" allowtransparency="true"></iframe>';
				}
                  echo "</div>"; ?>

            </div><!-- page-section -->

        <?php endif; ?>

<?php /*================== EVENT FOOTER ===================*/ ?>

      <footer class="article-footer">
        <div class="single-meta">
          <small><?php _e('Geplaatst op ','knob'); ?>
            <time class="updated entry-time" datetime="<?php get_the_time('Y-m-d'); ?>" itemprop="datePublished"><?php the_time(get_option('date_format')) ; ?></time>
          </small>
        </div>
      </footer>

</article>
